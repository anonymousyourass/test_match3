﻿using UnityEngine;
using UnityEngine.UI;
using Unity.Mathematics;
using System.Collections.Generic;
using System.Linq;

namespace Project
{
    public class Generator : MonoBehaviour
    {
        public GameObject prefab;
        public UnityEngine.Material prefabMaterialTile;

        

        public List<GameObject> cells;
        public UnityEngine.UI.Text textscore;

        public int score = 0;

        public void AddScore(int sc)
        {
            score += sc;
            textscore.text = score.ToString();
        }

        protected void OnEnable()
        {
            cells = new List<GameObject>();
            score = 0;
            textscore.text = score.ToString();
        }

        public void MatchScoreColor()
        {
            var score = cells.AddMatches();
            
            AddScore(score);
            cells.Where(c => c.HasC<Match>()).ToList().ForEach((m) =>
            {
                m.SetColorAlpha(Const.MATCHED_ALPHA_VALUE);
            });
        }

        private Color getColor(int rnd)
        {
            switch(rnd)
            {
                case (0):
                    return Color.blue;
                case (1):
                    return Color.yellow;
                case (2):
                    return Color.cyan;
                case (3):
                    return Color.magenta;
            }
            Debug.LogError("error generating random color : Generator");
            return Color.white;
        }

        public void SetRndView(GameObject e)
        {
            var rnd = UnityEngine.Random.Range(0, 4);
            e.SetColorAlpha(Const.DEFAULT_ALPHA_VALUE);
            var color = getColor(rnd);
            e.SetColor(color);
        }

        public void SetObsView(GameObject e)
        {
            e.SetColor(Color.red);
        }

        public void GenerateMatched()
        {
            foreach (var m in cells.Where(c => c.HasC<Match>()))
            {
                SetRndView(m);
            }
        }

        public void Unmatch()
        {
            var c = cells.Where(ce => ce.GetComponent<Match>() != null).Count();
            foreach (var m in cells)
            {
                GameObject.DestroyImmediate(m.GetComponent<Match>());
            }
        }


        public void Generate(int width,int height)
        {
            if (width < 2 || height < 2)
                return;

            foreach (var c in cells)
            {
                GameObject.Destroy(c);
            }

            cells = null;
            cells = new List<GameObject>();

            cells.Clear();

            score = 0;

            textscore.text = "Score : " + score;

            List<System.Tuple<int, int>> obstacleCoords = new List<System.Tuple<int, int>>();
            while (obstacleCoords.Count < 3)
            {
                var t = new System.Tuple<int, int>(UnityEngine.Random.Range(0, width), UnityEngine.Random.Range(0, height));
                if(obstacleCoords.Any(o => o.Item1 == t.Item1 && o.Item2 == t.Item2))
                {
                    continue;
                }
                obstacleCoords.Add(t);
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    var cell = Instantiate(prefab);
                    cell.name = i + " " + j;
                    cell.transform.position = new float3(i - width / 2, j - height / 2, 0f);
                    if(obstacleCoords.Any(o=>o.Item1 == i && o.Item2 == j))
                    {
                        var o = cell.AddComponent<Obstacle>();
                        o.enabled = false;
                        SetObsView(cell);
                        
                    }
                    else
                    {
                        SetRndView(cell);
                    }
                        
                    cells.Add(cell);
                    
                }
            }
        }
    }
}



