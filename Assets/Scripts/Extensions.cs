﻿using Unity.Collections;
using UnityEngine;
using Unity.Mathematics;
using System.Collections.Generic;
using System.Linq;

namespace Project
{
    public static class Extensions
    {
        public static T GetC<T>(this GameObject e) where T :  Component
        {
            return e.GetComponent<T>();
        }

        public static bool HasC<T>(this GameObject e) where T :  Component
        {
            return e.GetComponent<T>() != null;
        }

        public static bool HasNotC<T>(this GameObject e) where T :  Component
        {
            return e.GetComponent<T>() == null;
        }

        public static GameObject FirstWithCOrNew<T>(this List<GameObject> GameObjects) where T: Component
        {
            foreach (var e in GameObjects)
            {
                if(e.HasC<T>())
                    return e;
            }
            return null;
        }

        public static GameObject LastWithCOrNew<T>(this List<GameObject> GameObjects) where T : Component
        {
            for(int i = GameObjects.Count - 1; i>=0; i--)
            {
                if (GameObjects[i].HasC<T>())
                {
                    return GameObjects[i];
                }
            }

            return null;
        }

        public static float3 GetPos(this GameObject e)
        {
            return e.transform.position;
        }

        public static void SetPos(this GameObject e, float3 pos)
        {
            e.transform.position = pos;
        }

        public static void SetColor(this GameObject e, Color color)
        {
            if(e.HasC<ColorShade>())
            {
                var colorSh = e.GetC<ColorShade>();
                colorSh.col = color;
            }
            else
            {
                var c = e.AddComponent<ColorShade>();
                c.col = color;
                c.enabled = false;
            }
        }

        public static Color GetColor(this GameObject e)
        {
           return e.GetC<ColorShade>().col;
        }

        public static void SetColorAlpha(this GameObject e, float a)
        {
            if (e.HasC<ColorShade>())
            {
                var colorSh = e.GetC<ColorShade>();
                colorSh.col.a = a;
            }
            else
            {
                var colorShade = e.AddComponent<ColorShade>();
                colorShade.col.a = a;
                colorShade.enabled = false;
            }
        }

        public static bool IsDirNear(this GameObject e, GameObject e1)
        {
            return e.IsDirXNear(e1) || e.IsDirYNear(e1);
        }

        public static bool IsDiagTo(this GameObject e, GameObject e1)
        {
            var diff = e1.GetPos() - e.GetPos();
            return (math.length(diff) < 1.48f && math.length(diff) > 1.36f);
        }


        public static bool IsDirXNear(this GameObject e, GameObject e1)
        {
            return (math.abs(e.GetPos().x - e1.GetPos().x) < 1.1f && math.abs(e.GetPos().y - e1.GetPos().y) < 0.1f);
        }

        public static bool Is1StepBelow(this GameObject e, GameObject e1)
        {
            var diffY = e1.GetPos().y - e.GetPos().y;
            return diffY > 0.9f && diffY < 1.1f;
        }

        public static bool IsDirYNear(this GameObject e, GameObject e1)
        {
            return (math.abs(e.GetPos().y - e1.GetPos().y) < 1.1f && math.abs(e.GetPos().x - e1.GetPos().x) < 0.1f);
        }
     
        public static bool IsXSame(this GameObject e, GameObject e1)
        {
            return (math.abs(e.GetPos().x - e1.GetPos().x) < 0.1f);
        }

        public static bool IsYSame(this GameObject e, GameObject e1)
        {
            return (math.abs(e.GetPos().y - e1.GetPos().y) < 0.1f);
        }

        public static bool IsPosSame(this GameObject e, GameObject e1)
        {
            return e.IsXSame(e1) && e.IsYSame(e1);
        }

        public static bool IsPos(this GameObject e, float3 pos)
        {
            return (math.abs(e.GetPos().x - pos.x) < 0.1f) && (math.abs(e.GetPos().y - pos.y) < 0.1f);
        }


        public static bool IsDiagNear(this GameObject e, GameObject e1)
        {
            return math.length(e.To(e1)) > 1.1f && math.length(e.To(e1)) < math.sqrt(2.1f);
        }

        public static float3 To(this GameObject e, GameObject e1)
        {
            return e1.transform.position - e.transform.position;
        }



        public static bool AnyMatch(this List<GameObject> cells)
        {
            
            for (int i = 0; i < cells.Count; i++)
            {
                List<GameObject> sameColorDir = new List<GameObject>();
                cells.GetNeighboursDirSameColor(sameColorDir, cells[i], false);
                if (sameColorDir.Count > 2)
                {
                    return true;
                }

                sameColorDir = new List<GameObject>();
                cells.GetNeighboursDirSameColor(sameColorDir, cells[i], true);
                if (sameColorDir.Count > 2)
                {
                    return true;
                }
            }

           
            return false;
        }

        public static int AddMatches(this List<GameObject> cells)
        {
            var summScoreDir = 0;

            for (int i = 0; i < cells.Count; i++)
            {
                List<GameObject> sameColorDir = new List<GameObject>();
                cells.GetNeighboursDirSameColor(sameColorDir, cells[i], false);
                if(sameColorDir.Count > 2)
                {
                    sameColorDir.ForEach(e => 
                    {
                        var mat0 = e.AddComponent<Match>();
                        mat0.enabled =false;
                    });
                    summScoreDir = sameColorDir.Count * Const.SCORE_FOR_EACH_MATCHED_CELL - Const.SCORE_FOR_EACH_MATCHED_CELL;
                }

                sameColorDir = new List<GameObject>();
                cells.GetNeighboursDirSameColor(sameColorDir,cells[i], true);
                if (sameColorDir.Count > 2)
                {
                    sameColorDir.ForEach(e => 
                    {
                        var mat2 = e.AddComponent<Match>();
                        mat2.enabled = false;
                    } );
                    summScoreDir = sameColorDir.Count * Const.SCORE_FOR_EACH_MATCHED_CELL - Const.SCORE_FOR_EACH_MATCHED_CELL;
                }
            }
            return summScoreDir;
        }

        public static void GetNeighboursDirSameColor(this List<GameObject> es,List<GameObject> sameColDir, GameObject e, bool vertical)
        {
            foreach (var c in es)
            {
                if (c != e &&
                    sameColDir.Contains(c) == false &&
                    c.HasNotC<Obstacle>() &&
                    c.GetColor() == e.GetColor() &&
                    c.HasNotC<Match>())
                {
                    if (c.IsDirXNear(e) && vertical == false ||
                        c.IsDirYNear(e) && vertical)
                    {
                        sameColDir.Add(c);
                        es.GetNeighboursDirSameColor(sameColDir, c,vertical);
                    }
                }
            }
        }

        public static int CountBelowInARow<T>(this List<GameObject> cells,GameObject e) where T: Component
        {
            List<GameObject> obsToBtm = new List<GameObject>();
            cells.GetNeighboursToBtm<T>(obsToBtm, e);
            return obsToBtm.Count;
        }


        public static void GetNeighboursToBtm<T>(this List<GameObject> es, List<GameObject> obstColBtm, GameObject e) where T : Component
        {
            foreach (var c in es)
            {
                if (c!=e && 
                    obstColBtm.Contains(c) == false &&
                    c.HasC<T>() &&
                    c.Is1StepBelow(e))
                {
                    obstColBtm.Add(c);
                    es.GetNeighboursToBtm<T>(obstColBtm, c);
                }
            }
        }



        public static List<GameObject> GetNeighbours(this List<GameObject> es, GameObject e, bool diagonal = false)
        {
            List<GameObject> neighbours = new List<GameObject>();
            var posCell = e.transform.position;
            foreach (var c in es)
            {
                if (c != e && c.HasNotC<Obstacle>())
                {
                    if (c.IsDirNear(e) ||
                        diagonal && c.IsDiagNear(e))
                    {
                        neighbours.Add(c);
                    }
                }
            }
            return neighbours;
        }
        public static bool FindWith<T>(this List<GameObject> cells, GameObject from, GameObject cur, int recursionDeep) where T : Component
        {
            if (cur.HasC<Obstacle>() || recursionDeep <= 0)
                return false;

            if (cur.HasC<T>())
                return true;

            var neighbours = cells.GetNeighbours(cur);
            foreach (var neighbour in neighbours)
            {
                if (neighbour != from && cells.FindWith<T>(cur, neighbour, recursionDeep - 1))
                {
                    return true;
                }
            }

            return false;
        }


    }
}
