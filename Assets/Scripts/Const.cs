﻿using Unity.Mathematics;
namespace Project
{
    public class Const 
    {
        public const string COLOR_PRP_NAME = "_Color";
        public const string ENDGES_PRP_NAME = "_Count";

        public const float DEFAULT_ALPHA_VALUE = 1f;
        public const float SELECTED_ALPHA_VALUE = 0.01f;
        public const float MATCHED_ALPHA_VALUE = 0.0f;
        public const int SCORE_FOR_EACH_MATCHED_CELL = 5;


        public const float FALL_TIME = 1f;
        public const float SWITCH_MOVE_TIME = 1f;
        public const float SCALE_TIME = 1f;

        public static float3 DEFAULT_SCALE = new float3(1f, 1f, 1f);
        public static float3 MATCHED_SCALE = new float3(0.5f, 0.5f, 0.5f);
    }
}

