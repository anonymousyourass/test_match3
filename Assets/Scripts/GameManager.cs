﻿using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;
using DG.Tweening;

namespace Project
{
    using FluidSim2DProject;
    public class GameManager : MonoBehaviour
    {
        //public UnityEngine.UI.InputField inputW;
        //public UnityEngine.UI.InputField inputH;

      
		
		public float uvScaleX = 0.5f;
        public float uvScaleY = 0.5f;
		
		public float uvScaleXOff = 0.25f;
        public float uvScaleYOff = 0.25f;
		
        public float radMul = 1f;



        public Generator gen;
        public RayTraceMaster render;
        public FluidSim fluidRender;
        public UnityEngine.UI.Button btnGenerate;

        private void Start()
        {
            disabledEnd();
        }

        void OnEnable()
        {
            btnGenerate.onClick.AddListener(disabledEnd);
        }

        void OnDisable()
        {
            UnityEngine.UI.Button btn1 = btnGenerate.GetComponent<UnityEngine.UI.Button>();
            btnGenerate.onClick.RemoveAllListeners();

            render.spheresPositions = new List<Vector4>();
            render.spheresRadiuses = new List<float>();
            render.spheresAlbedos = new List<Vector4>();
            render.spheresSpeculars = new List<Vector4>();

            fluidRender.spheresPositions =  new List<Vector4>();
            fluidRender.spheresRadiuses = new List<float>();

        }

        private void disabledEnd()
        {
            //int.TryParse(inputW.text, out var width);
            //int.TryParse(inputH.text, out var height);

            int width = 4;
            int height = 4;
            gen.Generate(width, height);
            if (gen.cells.AnyMatch())
            {
                gen.MatchScoreColor();
                gen.GenerateMatched();
                fall();
            }

            render.spheresPositions = new List<Vector4>();
            render.spheresRadiuses = new List<float>();
            render.spheresAlbedos = new List<Vector4>();
            render.spheresSpeculars = new List<Vector4>();

            fluidRender.spheresPositions = new List<Vector4>();
            fluidRender.spheresRadiuses = new List<float>();

            for (int i = 0; i < 16; i++)
            {
                render.spheresPositions.Add(Vector4.zero);
                render.spheresRadiuses.Add(0.5f);
                render.spheresAlbedos.Add(Vector4.zero);
                render.spheresSpeculars.Add(Vector4.zero);
				render.smoothnesses.Add(0.0f);
				render.emissionses.Add(0.0f);

                fluidRender.spheresPositions.Add(Vector4.zero);
                fluidRender.spheresRadiuses.Add(0.05f);
            }
        }

        private float3 fallPos(GameObject e)
        {
            var topToB = gen.cells.OrderByDescending(c => c.GetPos().y).ToList();
            float3 downM = new float3(0f, -1, 0f);

            if (e.HasC<Match>())
            {
                var colMatchedCells = topToB.Where(c => c.IsXSame(e) && c.HasC<Match>()).ToList();
                var matchShift =  colMatchedCells.IndexOf(e);
                var top0 = topToB.First(c => c.IsXSame(e)).GetPos();
                var shiftM = getShifted(matchShift, top0);
               
                return shiftM;
            }
            var thisColMatchedB = topToB.Where(c => c != e && c.IsXSame(e) && c.GetPos().y < e.GetPos().y && c.HasC<Match>());
            var mbc = thisColMatchedB.Count();
            var shift = getShifted(mbc,e.GetPos());
            
            return shift;

        }

        private float3 getShifted(int matchShift,float3 topPos)
        {
            var topToB = gen.cells.OrderByDescending(c => c.GetPos().y).ToList();
            var top = topToB.First(c=>c.IsPos(topPos));
            var posTM = getPosShifted(topPos,matchShift);
            var obstCount = topToB.Count(c=>c.HasC<Obstacle>() && c.IsXSame(top) &&
            c.GetPos().y >= posTM.y && c.GetPos().y <= top.GetPos().y);

            var posTMC = getPosShifted(topPos,matchShift + obstCount);

            var tmc = topToB.First(c => c.IsPos(posTMC));
            if (tmc.HasC<Obstacle>())
            {
                var cdbtmc = gen.cells.CountBelowInARow<Obstacle>(tmc);
                var posTMCB = getPosShifted(topPos, matchShift + obstCount+ cdbtmc + 1);
                return posTMCB;
            }

            return posTMC;
        }

        private float3 getPosShifted(float3 pos,int shift)
        {
            float3 down = new float3(0f, -1, 0f);
            return pos + down * shift;
        }

        private void fall()
        {
			render.DropSamples();
            isMovingCells = true;
            var topToB = gen.cells.OrderByDescending(e => e.GetPos().y).ToList();
            var float3Res = new List<float3>();
            for (int i = 0; i < topToB.Count; i++)
            {
                var e = topToB[i];
                if (e.HasC<Obstacle>())
                {
                    float3Res.Add(e.GetPos());
                }
                else
                {
                    var posWithNoObs = fallPos(e);
                    float3Res.Add(posWithNoObs);
                }
            }
            var isFallingCells = new bool[topToB.Count];
            var completeIdx = 0;
            for (int i = 0; i < topToB.Count; i++)
            {
                var e = topToB[i];

                e.transform.DOMove(float3Res[i], 1f).OnComplete(()=> 
                {
                    isFallingCells[completeIdx] = true;
                    completeIdx++;
                    if(completeIdx == isFallingCells.Length - 1)
                    {
                        gen.Unmatch();
						if (gen.cells.AnyMatch())
						{
							gen.MatchScoreColor();
							gen.GenerateMatched();
							fall();
						}
						else
						{
							isMovingCells = false;
						}
                    }
                });
            }
        }

        private void switchMoveInTime(float time, GameObject e1, GameObject e2, bool recursive)
        {
			render.DropSamples();
            isMovingCells = true;
            bool[] isSwitchingCells = new bool[2] { true,true};
            e1.transform.DOMove(e2.transform.position, time).OnComplete(()=> 
            {
                isSwitchingCells[0] = false;
                if (isSwitchingCells[1] == false)
                {
					if (gen.cells.AnyMatch())
					{
						gen.MatchScoreColor();
						gen.GenerateMatched();
						fall();
					}
					else if (recursive)
					{
						switchMoveInTime(Const.SWITCH_MOVE_TIME, e1, e2, false);
					}
					else
					{
						isMovingCells = false;
					}
                }
            });

            e2.transform.DOMove(e1.transform.position, time).OnComplete(() =>
            {
                isSwitchingCells[1] = false;
                if (isSwitchingCells[0] == false)
                {
                    if (gen.cells.AnyMatch())
					{
						gen.MatchScoreColor();
						gen.GenerateMatched();
						fall();
					}
					else if (recursive)
					{
						switchMoveInTime(Const.SWITCH_MOVE_TIME, e1, e2, false);
					}
					else
					{
						isMovingCells = false;
					}
                }
            });
        }

        //isMoving... isSwitching.. isFalling... замена Tween.IsPlaying или HasC<TweenMove> первого нет второй не дописан
        //с ключом компилятору TWEEN_AS_COMPONENT появляются баги 
        private bool isMovingCells = false; 

        void Update()
        {
            if (isMovingCells == false)
            {
				if (isUserInput()) //нет матчей есть возможные матчи - ходит игрок
                {
                    GameObject e = null;
                    if (Raycast(ref e))
                    {
                        handleSelection(e);
                    }
                }
            }

            if(gen.cells.Count > 15)
            {
                for (int i = 0; i < 16; i++)
                {
                    var tr = gen.cells[i].transform;
                    render.spheresPositions[i] = new Vector4(tr.position.x, tr.position.y, tr.position.z, 0f);
                    var color = gen.cells[i].GetColor();
                    render.spheresAlbedos[i] = new Vector4(color.r, color.g, color.b, 0f);
					render.smoothnesses[i] = 0.01f;
					render.emissionses[i] = (1.0f - color.a) + 0.2f;
                    render.spheresSpeculars[i] = new Vector4(1.0f - color.a/2f , 1.0f - color.a / 2f, 1.0f - color.a / 2f, 0f);

                    fluidRender.spheresPositions[i] = new Vector4(
						((tr.position.x)/uvScaleX)+uvScaleXOff,
						((tr.position.y)/ uvScaleY)+uvScaleYOff,
						0f,
						0f);
                    fluidRender.spheresRadiuses[i] = 0.05f*radMul;
                }
            }
        }
        private bool isUserInput()
        {
#if UNITY_EDITOR || UNITY_WEBGL
            return Input.GetMouseButtonDown(0);
#else
		    return Input.touchCount == 1;
#endif
        }

        private bool Raycast(ref GameObject hitted)
        {
            var screenP = getPos();
            foreach (var c in gen.cells)
            {
                var wtosL = Camera.main.WorldToScreenPoint(c.transform.position + Vector3.left * 0.5f);
                var wtosR = Camera.main.WorldToScreenPoint(c.transform.position + Vector3.right * 0.5f);
                var wtosD = Camera.main.WorldToScreenPoint(c.transform.position + Vector3.down * 0.5f);
                var wtosU = Camera.main.WorldToScreenPoint(c.transform.position + Vector3.up * 0.5f);

                if (wtosL.x <= screenP.x &&
                    wtosR.x  >= screenP.x &&
                    wtosU.y  >= screenP.y &&
                    wtosD.y <= screenP.y)
                {
                    hitted = c;
					Debug.Log("L " + wtosL + "R " + wtosR + "U " + wtosU  + "D " + wtosD );
                    return true;
                }
            }
            return false;
        }

        private Vector3 getPos()
        {
#if UNITY_EDITOR || UNITY_WEBGL
            return new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
#else
			return new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 0);
#endif
        }

        private void handleSelection(GameObject e)
        {
            if (e.HasC<Obstacle>())
                return;

            if (gen.cells.Count(GameObject => GameObject.HasC<Selected>()) > 1)
                return;

            if (gen.cells.Any(c => c.HasC<Selected>()))
            {
                var selected = gen.cells.First(c => c.HasC<Selected>());
                if (selected == e)
                {
                    GameObject.Destroy(selected.GetComponent<Selected>());
                    selected.SetColorAlpha(Const.DEFAULT_ALPHA_VALUE);
                }
                else
                {
                    if (selected.IsDirNear(e))
                    {
                        GameObject.Destroy(selected.GetComponent<Selected>());
                        selected.SetColorAlpha(Const.DEFAULT_ALPHA_VALUE);
                        e.SetColorAlpha(Const.DEFAULT_ALPHA_VALUE);

                        switchMoveInTime(Const.SWITCH_MOVE_TIME, selected, e, false);
                    }
                    else
                    {
                        GameObject.Destroy(selected.GetComponent<Selected>());
                        selected.SetColorAlpha(Const.DEFAULT_ALPHA_VALUE);
                        var s = e.AddComponent<Selected>();
                        s.enabled = false;
                        e.SetColorAlpha(Const.SELECTED_ALPHA_VALUE);
                    }
                }
            }
            else
            {
                var sel = e.AddComponent<Selected>();
                sel.enabled = false;
                e.SetColorAlpha(Const.SELECTED_ALPHA_VALUE);
            }
           
        }
    }
}

