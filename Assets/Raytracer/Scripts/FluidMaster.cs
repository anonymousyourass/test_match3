﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FluidMaster : MonoBehaviour
{
   
    public Material FluidMaterial;

    public Texture2D inputVelocity;
    public Texture2D advected;

    public RenderTexture _advection;


    private Camera _camera;


    private void OnEnable()
    {
        FluidMaterial.SetTexture("_MainTex", inputVelocity);
        FluidMaterial.SetTexture("_Advected", advected);
    }

    private void OnDisable()
    {
        
    }

  
    private void Awake()
    {
        _camera = GetComponent<Camera>();
    }

    private void Update()
    {
        if (transform.hasChanged)
        {
            transform.hasChanged = false;
        }

    }

    private void SetShaderParameters()
    {

        

    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        SetShaderParameters();

        Graphics.Blit(inputVelocity, _advection, FluidMaterial);
        Graphics.Blit(_advection, destination);

    }
}