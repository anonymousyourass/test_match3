﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RayTraceMaster : MonoBehaviour
{
    public Light DirectionalLight;

    public Material RayTracingMaterial;
    public Material AAMaterial;
	
    public Texture2D SkyboxTexture;
    public RenderTexture _target;
	public RenderTexture _aatarget;
	
    private Camera _camera;

    public FluidSim2DProject.FluidSim fluuidSim;
    private uint _currentSample = 0;
    public Material addingMaterial;

    public List<Vector4> spheresPositions;
    public List<float> spheresRadiuses;
    public List<Vector4> spheresAlbedos;
    public List<Vector4> spheresSpeculars;
	public List<float> smoothnesses;
	public List<float> emissionses;

	public float mX;
	public float sampleMX = 1f;
	
    private void OnEnable()
    {
        _currentSample = 0;
		RayTracingMaterial.SetTexture("_SkyboxTexture",SkyboxTexture);	//fluuidSim.gameObject.GetComponent<GUITexture>().texture
    }
	
    private void OnDisable()
    {

    }

	public void DropSamples()
	{
		_currentSample =0;
	}
	
    private void Awake()
    {
        _camera = GetComponent<Camera>();
    }

    private void Update()
    {
        if (transform.hasChanged)
        {
            _currentSample = 0;
            transform.hasChanged = false;
        }

    }

    private void SetShaderParameters()
    {
		
        RayTracingMaterial.SetMatrix("_CameraTWorld", _camera.cameraToWorldMatrix);
        RayTracingMaterial.SetMatrix("_CameraInverseProjection", _camera.projectionMatrix.inverse);
        
        RayTracingMaterial.SetVector("_PixelOffset", new Vector2(UnityEngine.Random.value*mX, UnityEngine.Random.value*mX));
        Vector3 l = DirectionalLight.transform.forward;
        RayTracingMaterial.SetVector("_DirectionalLight", new Vector4(l.x, l.y, l.z, DirectionalLight.intensity));
		RayTracingMaterial.SetFloat("_Seed", Random.value);
		
        if(spheresAlbedos.Count > 15)
        {
            RayTracingMaterial.SetVectorArray("_albedos", spheresAlbedos);
            RayTracingMaterial.SetFloatArray("_radiuses", spheresRadiuses);
            RayTracingMaterial.SetVectorArray("_speculars", spheresSpeculars);
            RayTracingMaterial.SetVectorArray("_positions", spheresPositions);
			RayTracingMaterial.SetFloatArray("_smoothnesses", smoothnesses);
			RayTracingMaterial.SetFloatArray("_emissionses", emissionses);
        }
        addingMaterial.SetTexture("_Spheres", _target);
		AAMaterial.SetFloat("_Sample",_currentSample*sampleMX);
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        SetShaderParameters();
		Graphics.Blit(_target, _target,RayTracingMaterial);
		//Graphics.Blit(fluuidSim.gameObject.GetComponent<GUITexture>().texture,_target, addingMaterial);
		Graphics.Blit(_target,_aatarget,AAMaterial);
		Graphics.Blit(_aatarget, destination);

        _currentSample++;
    }
}