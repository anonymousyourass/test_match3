﻿// Upgrade NOTE: replaced 'samplerRECT' with 'sampler2D'
// Upgrade NOTE: replaced 'texRECT' with 'tex2D'

Shader "Unlit/fluid"
{
    Properties
    {
		_MainTex("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
		Cull Off ZWrite Off ZTest Always
		Blend SrcAlpha OneMinusSrcAlpha
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

			Texture2D<float4> _Spheres;
			SamplerState sampler_Spheres;
			
			

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }


			float4 frag(v2f i) : SV_Target
			{
				float4 spheres = _Spheres.Sample(sampler_Spheres, i.uv);
				float4 fluid = tex2D(_MainTex, i.uv);
				if ((fluid.g <= 0.0 || fluid.b <= 0.0) && fluid.r > 0.0f)
				{
					spheres = lerp(spheres, fluid,fluid.r);
				}
				
				return spheres;
			}


            ENDCG
        }
    }
}
