// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "FluidSim/Obstacle" 
{
	SubShader 
	{
    	Pass 
    	{
			ZTest Always

			CGPROGRAM
			#include "UnityCG.cginc"
			#pragma target 2.5
			#pragma vertex vert
			#pragma fragment frag
			
			uniform float2 _InverseSize;


			#define _sphereCount       16
			#define _zeroC       0
			#define _oneC       1

			float4 _Point[_sphereCount];
			float _Radius[_sphereCount];
		
			struct v2f 
			{
    			float4  pos : SV_POSITION;
    			float2  uv : TEXCOORD0;
			};

			v2f vert(appdata_base v)
			{
    			v2f OUT;
    			OUT.pos = UnityObjectToClipPos(v.vertex);
    			OUT.uv = v.texcoord.xy;
    			return OUT;
			}
			
			float4 frag(v2f IN) : COLOR
			{
				float4 result = float4(0,0,0,0);
				
				//draw border 
				if(IN.uv.x <= _InverseSize.x) result = float4(1,1,1,1);
				if(IN.uv.x >= 1.0-_InverseSize.x) result = float4(1,1,1,1);
				if(IN.uv.y <= _InverseSize.y) result = float4(1,1,1,1);
				if(IN.uv.y >= 1.0-_InverseSize.y) result = float4(1,1,1,1);
				
				//draw point
				for (int i= _zeroC;i< _sphereCount;i+= _oneC)
				{
					float d = distance(_Point[i], IN.uv);
					if (d < _Radius[i]) return float4(1, 1, 1, 1);
				}

				
				return result;
			}
			
			ENDCG

    	}
	}
}